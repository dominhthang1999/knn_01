import csv
import pandas as pd
import time
import sys


def read_file(path_file):
    """
    Read file from path file (type file is csv) and convert it to DataFrame
    :param path_file: path file input (String)
    :return: Object (DataFrame)
    """

    with open(path_file) as csv_file:
        data_file = csv.reader(csv_file)
        # Assign name of columns
        dfObj = pd.DataFrame(data_file, columns=['buying', 'maint', 'doors', 'persons', 'lug_boot', 'safety', 'result'])
        return dfObj


def _assign_weight_attributes(data_frame, column_name):
    """
    Convert character value from one column in data_frame to numeric value to easy calculator distance.
    :param data_frame: DataFrame be need assign weight (DataFrame)
    :param column_name: Name of column you want convert string to weight (String)
    :return: No return (change data in data_frame)
    """

    # Get domain value in column_name
    duplicateRowsDF = data_frame[~data_frame[column_name].duplicated()][column_name]
    weight_value = 0
    # List of pairs character key and numeric value
    list_weight = []
    for value in duplicateRowsDF:

        if column_name == 'result':
            if value == 'unacc':
                list_weight.append([value, 0])
            else:
                list_weight.append([value, 1])
        else:
            list_weight.append([value, weight_value])
            weight_value += 1
    # Replace character key by numeric value from list_weight
    key = 0
    value = 1
    for temp in list_weight:
        data_frame[column_name] = data_frame[column_name].replace(to_replace=temp[key], value=temp[value])


def assign_weight_attributes(data_frame):
    """
    Convert character value in all column in data_frame to numeric value to easy calculator distance.
    :param data_frame: DataFrame be need assign weight (DataFrame)
    :return: No return (change data in data_frame)
    """
    for column_name in data_frame:
        _assign_weight_attributes(data_frame, column_name)


def _distance_two_point(point_1, point_2, p_norm=2):
    """
    Calculator distance from two data (two point)
    :param point_1:
    :param point_2:
    :param p_norm: (Int)
            p_norm=1, the distance measure is the Manhattan measure.
            p_norm=2, the distance measure is the Euclidean measure.
            p_norm = ∞, the distance measure is the Chebyshev measure.
    :return: Distance two data (Float)
    """

    if len(point_1) == len(point_2):
        len_val = len(point_1)
        result = sum((point_1[column] - point_2[column])**p_norm for column in range(len_val - 1))**(1/p_norm)
        return result
    else:
        raise TypeError


def distance_point_to_dataframe(point, data_frame, p_norm=2):
    """
    Calculator distance from one point to list point.
    :param point:
    :param data_frame: (DataFrame)
    :param p_norm: Default 2. Type you want calculator distance (Int)
    :return: Data of list point and have column 'distance' in the end of column (DataFrame)
    """
    result = data_frame.copy()
    list_distance = []
    for index in range(len(data_frame)):
        list_distance.append(_distance_two_point(point, result.iloc[index], p_norm))
    # add column 'distance' in result
    result['distance'] = list_distance
    return result.sort_values(by=['distance'])


def get_mode_predict_one_point(point, train_set, k=10, p_norm=2):
    """
    Get mode for one point. Predict the result of point
    :param point:
    :param train_set: (DataFrame)
    :param k: Default = 10. Number neighbor you want (Int)
    :param p_norm: Default = 2 (Euclidean measure). Type you want calculator distance (Int)
    :return:  Point with new result
    """
    dataframe = distance_point_to_dataframe(point, train_set, p_norm).head(k)
    point_r = point.copy()
    point_r['result'] = dataframe['result'].mode().iloc[0]
    return point_r


def divide_data_set(data_frame, ratio=0.7):
    """
    Create training set and testing set from data_frame with ratio default 0.7
    :param data_frame: (DataFrame)
    :param ratio: Default training set: 70%, testing set: 30% (Float, [0:1])
    :return: training set and testing set by order (Two set, each set type DataFrame)
    """
    train_set = data_frame.sample(frac=ratio, replace=True)
    test_set = data_frame.drop(train_set.index)
    return train_set, test_set


def KNN_predict(train_set, _predict_set, k=10, p_norm=2):
    """
    Predict
    :param train_set: (DataFrame)
    :param _predict_set: (DataFrame)
    :param k: Default 10. Number neighbor you want (Int)
    :param p_norm: Default 2 (Euclidean measure) . Type you want calculator distance (Int)
    :return: Predict set with result be predicted (DataFrame)
    """
    predict_set = _predict_set.copy()
    for row_predict in range(len(predict_set)):
        predict_set.iloc[row_predict] = get_mode_predict_one_point(predict_set.iloc[row_predict], train_set, k, p_norm)
        
        # Print progress
        sys.stdout.write('\r')
        j = (row_predict + 1) / len(predict_set)
        # the exact output you're looking for:
        sys.stdout.write("[%-70s] %d%%" % ('='*int(70*j), 100*j))
        sys.stdout.flush()
    return predict_set


def get_confusion_matrix(actual, predicted):
    confusion_matrix = pd.crosstab(actual['result'], predicted['result'], rownames=['Actual'], colnames=['Predicted'])
    print(confusion_matrix)
    import seaborn as sn
    import matplotlib.pyplot as plt
    sn.heatmap(confusion_matrix, annot=True)
    plt.show()
    return confusion_matrix


def get_evaluate(confusion_matrix):
    TN = confusion_matrix[0][0]
    FP = confusion_matrix[0][1]
    FN = confusion_matrix[1][0]
    TP = confusion_matrix[1][1]
    accuracy = (TN + TP)*100 / (TN + FP + FN + TP)
    precision = TP * 100/(TP + FP)
    recall = TP*100/(TP + FN)
    f1_score = 2 * (precision * recall)/(precision + recall)
    return accuracy, precision, recall, f1_score


def main():
    print('\n... Start ...\n')

    # Process data and normalize it
    path_file = './car.csv'
    data_frame = read_file(path_file)
    assign_weight_attributes(data_frame)
    # --- End of Process ---

    ratio = 0.8
    print("Training set: %d\nTesting set: %d\n" % (round(ratio*100), round((1 - ratio)*100)))
    train_set, test_set = divide_data_set(data_frame, ratio=ratio)

    predict_test = test_set.copy()
    p_norm = 2

    for i in range(10, 11):
        start = time.time()
        k = i
        print("K: %d\np_norm: %d" % (k, p_norm))
        print('\n... Running ...')
        predict_test = KNN_predict(train_set=train_set, _predict_set=predict_test, k=k, p_norm=p_norm)

        print('\nTime run: %.2f s' % (time.time() - start))

        confusion_matrix = get_confusion_matrix(test_set, predict_test)
        accuracy, precision, recall, f1_score = get_evaluate(confusion_matrix)
        print('\nAccuracy : %.2f%%' % accuracy)
        print("Precision: %.2f%%" % precision)
        print("recall: %.2f%%" % recall)
        print("f1_score: %.2f" % f1_score)

        print('-------- DONE --------\n')


if __name__ == "__main__":
    main()
